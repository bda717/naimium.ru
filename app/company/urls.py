from django.urls import path

from .views import DepartmentsListView, EmployeesListView, EmployeesDetailView

app_name = 'company'

urlpatterns = [
    path('employees', EmployeesListView.as_view()),
    path('employees/<int:pk>', EmployeesDetailView.as_view()),
    path('departments', DepartmentsListView.as_view()),
]
