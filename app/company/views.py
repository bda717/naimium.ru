from rest_framework.generics import ListAPIView, CreateAPIView, DestroyAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAdminUser

from .models import Departments, Employees
from .serializers import DepartmentsSerializer, EmployeesSerializer


class Pagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 100


class DepartmentsListView(ListAPIView):
    queryset = Departments.objects.all()
    serializer_class = DepartmentsSerializer


class EmployeesListView(ListAPIView, CreateAPIView):
    queryset = Employees.objects.all()
    serializer_class = EmployeesSerializer
    permission_classes = [IsAdminUser]
    pagination_class = Pagination

    def get_queryset(self):
        queryset = Employees.objects.all()
        department_id = self.request.query_params.get('department')
        surname = self.request.query_params.get('surname')
        if department_id:
            try:
                int(department_id)
                queryset = queryset.filter(department_id=department_id)
            except ValueError:
                queryset = queryset.none()
        if surname:
            queryset = queryset.filter(full_name__istartswith=surname)
        return queryset


class EmployeesDetailView(DestroyAPIView):
    queryset = Employees.objects.all()
    permission_classes = [IsAdminUser]
    serializer_class = EmployeesSerializer
