from django.db import models


class Employees(models.Model):
    full_name = models.CharField('ФИО', max_length=255, db_index=True)
    image = models.FileField('Фото', max_length=255)
    position = models.CharField('Должность', max_length=255)
    salary = models.DecimalField('Оклад', max_digits=7, decimal_places=2)
    age = models.PositiveSmallIntegerField('Возраст')
    department = models.ForeignKey('Departments', verbose_name='Департамент', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'

    def __str__(self):
        return self.full_name


class Departments(models.Model):
    name = models.CharField('Название', max_length=255)
    director = models.OneToOneField(Employees, verbose_name='Директор', null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Департамент'
        verbose_name_plural = 'Департаменты'

    def __str__(self):
        return self.name
