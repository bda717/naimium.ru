from django.contrib import admin

from .models import Departments, Employees


@admin.register(Departments)
class DepartmentsAdmin(admin.ModelAdmin):
    list_display = ('name', 'director')


@admin.register(Employees)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'image', 'position', 'salary', 'age', 'department')
