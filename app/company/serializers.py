from django.db.models import Avg
from rest_framework import serializers

from .models import Departments, Employees


class DepartmentsSerializer(serializers.ModelSerializer):
    count = serializers.SerializerMethodField()
    salary = serializers.SerializerMethodField()

    class Meta:
        model = Departments
        fields = '__all__'

    def get_count(self, instance):
        return Employees.objects.filter(department=instance).count()

    def get_salary(self, instance):
        return Employees.objects.filter(department=instance).aggregate(Avg('salary'))


class EmployeesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employees
        fields = '__all__'
